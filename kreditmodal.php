<?php
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
$id=$_POST['id'];
$data=$aju->get_aju_by_id($id);
$us=$aju->userdata($data['ID_User']);
$kend=$aju->kenddata($data['Kd_Kendaraan']);
$jenis=$aju->jenisdata($kend['Kd_Jenis']);
?>
<html>
<body>
<h3 style="text-align: center;">Ubah Status Pengajuan Kredit <br> Petugas 1</h3>  
<table class="stripped" style="font-size: 14px;">
<tr>
    <td width="50%"><b>Nama</b></td>
    <td width="10%" align="middle">:</td>
     <td><i><?php echo $us['Name'];?></i></td>
</tr>
<tr>
    <td><b>Asal Kota</b></td>
    <td align="middle">:</td>
     <td><i><?php echo $us['City'];?></i></td>
</tr>
<tr>
    <td><b>Asal Negara</b></td>
    <td align="middle">:</td>
     <td><i><?php echo $us['Country'];?></i></td>
</tr>
<tr>
    <td><b>Penghasilan</b></td>
    <td align="middle">:</td>
     <td><i>Rp. <?php echo number_format($us['Penghasilan'],0,",",".");?></i></td>
</tr>
<tr>
    <td><b>Jenis Kendaraan</b></td>
    <td align="middle">:</td>
     <td><i><?php echo $jenis['Nm_Jenis'];?></i></td>
</tr>
<tr>
    <td><b>Waktu Pengajuan</b></td>
    <td align="middle">:</td>
     <td><b><i><?php echo date('d-M-Y',$data['Tgl_Aju']);?></i></b></td>
</tr>
<tr>
    <td><b>Nama Kendaraan</b></td>
    <td align="middle">:</td>
     <td><b><i><?php echo $kend['Nm_Kendaraan'];?></i></b></td>
</tr>
<tr>
    <td><b>Harga Kendaraan</b></td>
    <td align="middle">:</td>
     <td><b><i>Rp. <?php echo number_format($kend['Harga'],0,",",".");?></i></b></td>
</tr>
<tr >
    <td ><b>Status</b></td>
    <td align="middle">:</td>
    <td ><b><?php echo $data['Status'];?></b></td>
</tr>
</table>
<h4>Klik tombol dibawah ini untuk mengubah status Pengajuan Kredit</h4>
<div class="panel panel-default" style="margin-top: 5px; width: 50%; margin-left: auto; margin-right:auto;">
        <div class="panel-heading"><h6 class="panel-title">Kontrol Status</h6>
        </div>
        <div class="panel-body">  
        <div class="tombol1" align="middle">  
        <?php 
        echo '<a href="prosestgs1.php?id='.$data['ID_Aju'].'&act=approve" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> Approve 
            </a>';?>      
            <?php 
        echo '<a href="prosestgs1.php?id='.$data['ID_Aju'].'&act=cancel" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Cancel</a>';
        ?>
        </div>
    </div>
</div>
<script type="text/javascript">      
 
</script>
</body>
</html>