<?php 
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
if($users['Level'] != "Manager")
{ 	exit("You don't have permission to access this page!"); }
if(isset($_GET['eror'])){
	$errors[]=$_GET['eror'];
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css"/>
	<style type="text/css">
		body{background: #f7f7f7 url('images/body-bg.png');}
		.container {margin: 50px 2px; width: 100%;}
		.eroran{margin: 20px;}
	</style>
	<script src="js/jquery-1.11.2.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="datatables/jquery.dataTables.js"></script>
    <script src="datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
       function delete_confirm(link) {
		var msg = confirm('Apakah anda yakin akan menghapus data tersebut?');
		if(msg == false) {
			return false;
		}
	}
</script>

</head>
<body>
<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ubah Status Pengajuan Kredit</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
	<ul class="breadcrumb navbar-fixed-top"><li>Manager</li><li class="active">Assignment</li></ul>
	<div class="container">
	<?php 
	if(empty($errors) === false){
		echo '<div class="eroran">'.
				   '<div class="alert alert-danger alert-error">'.
				        '<a href="#" class="close" data-dismiss="alert">&times;</a>'.
				       '<strong>Error! </strong>'.implode($errors). 
				   '</div>'.
				'</div>';
	}
	?>
	<h3>Assignment Manager</h3>
	<table id="lookup" class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
        	<th>No.</th>
        	<th>Customer</th>
            <th>Kendaraan</th>
            <th>Jenis</th>
            <th>Harga</th>
            <th>Waktu Pengajuan</th>
            <th>Proses</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
		<?php 
		$info=$aju->get_aju_by_status("Pemeriksaan 2");
		$a=1;
		foreach ($info as $sch) {
			$us=$aju->userdata($sch['ID_User']);
			$kend=$aju->kenddata($sch['Kd_Kendaraan']);
			$jenis=$aju->jenisdata($kend['Kd_Jenis']);
			if ($sch['Status_Ptgs1']=="Approved" && $sch['Status_Ptgs2']=="Approved"){
			echo '<tr><td align="middle">'.$a.'</td>'.
				 '<td>'.$us['Name'].'</td>'.
				 '<td>'.$kend['Nm_Kendaraan'].'</td>'.
				 '<td align="middle">'.$jenis['Nm_Jenis'].'</td>'.	
				 '<td align="right">Rp. '.number_format($kend['Harga'],0,",",".").'</td>'.
				 '<td align="middle">'.date('d-M-Y',$sch['Tgl_Aju']).'</td>'.
				 '<td align="middle">'.$sch['Status'].'</td>'.
				 '<td align="middle">'.
				 '<a href="#" id="buton" data-toggle="modal" data-target="#myModal" data-id="'.$sch['ID_Aju'].'" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-list"></span> Ubah Status</a>'.
				 '</td></tr>';
				 $a+=1;}
		}
		?>
    </tbody>
	</table>
	<p>&nbsp;</p>
 </div>
 <script type="text/javascript">
       $(document).ready(function(){
		$('#lookup').dataTable({
		});			
	})

       $(function(){
            $(document).on('click','#buton',function(e){
                e.preventDefault();
                $.post('kreditmodal3.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                ); $("#myModal").modal('show');
            });
        });
 </script>
</body>
</html>