<?php 
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
if($users['Level'] != "Petugas1" && $users['Level'] != "Petugas2" && $users['Level'] != "Manager")
{ 	exit("You don't have permission to access this page!"); }
if(isset($_GET['eror'])){
	$errors[]=$_GET['eror'];
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css"/>
	<style type="text/css">
		body{background: #f7f7f7 url('images/body-bg.png');}
		.container {margin: 50px 2px; width: 100%;}
		.eroran{margin: 20px;}
	</style>
	<script src="js/jquery-1.11.2.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="datatables/jquery.dataTables.js"></script>
    <script src="datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
       function delete_confirm(link) {
		var msg = confirm('Apakah anda yakin akan menghapus data tersebut?');
		if(msg == false) {
			return false;
		}
	}
</script>

</head>
<body>
<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ubah Status Pengajuan Kredit</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
	<ul class="breadcrumb navbar-fixed-top"><li><?php if ($users['Level']=="Petugas1"){echo "Petugas 1";}else if($users['Level']=="Petugas2"){echo "Petugas 2";}else{echo "Manager";}?></li><li class="active">Semua Pengajuan</li></ul>
	<div class="container">
	<?php 
	if(empty($errors) === false){
		echo '<div class="eroran">'.
				   '<div class="alert alert-danger alert-error">'.
				        '<a href="#" class="close" data-dismiss="alert">&times;</a>'.
				       '<strong>Error! </strong>'.implode($errors). 
				   '</div>'.
				'</div>';
	}
	?>
	<h3>Semua Pengajuan</h3>
	<table id="lookup" class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
        	<th rowspan="2">No.</th>
        	<th rowspan="2">Customer</th>
            <th rowspan="2">Kendaraan</th>
            <th rowspan="2">Jenis</th>
            <th rowspan="2">Harga</th>
            <th rowspan="2">Waktu Pengajuan</th>
            <th rowspan="2">Proses</th>
            <th colspan="3">Status</th>
        </tr>
        <tr>
        	<th>Petugas 1</th>
        	<th>Petugas 2</th>
        	<th>Manager</th>
        </tr>
    </thead>
    <tbody>
		<?php 
		$info=$aju->get_aju_all();
		$a=1;
		foreach ($info as $sch) {
			$us=$aju->userdata($sch['ID_User']);
			$kend=$aju->kenddata($sch['Kd_Kendaraan']);
			$jenis=$aju->jenisdata($kend['Kd_Jenis']);
			echo '<tr><td align="middle">'.$a.'</td>'.
				 '<td>'.$us['Name'].'</td>'.
				 '<td>'.$kend['Nm_Kendaraan'].'</td>'.
				 '<td align="middle">'.$jenis['Nm_Jenis'].'</td>'.	
				 '<td align="right">Rp. '.number_format($kend['Harga'],0,",",".").'</td>'.
				 '<td align="middle">'.date('d-M-Y',$sch['Tgl_Aju']).'</td>'.
				 '<td align="middle">'.$sch['Status'].'</td>'.
				 '<td align="middle">'.$sch['Status_Ptgs1'].'</td>'.
				 '<td align="middle">'.$sch['Status_Ptgs2'].'</td>'.
				 '<td align="middle">'.$sch['Status_Manager'].'</td>'.
				 '</tr>';
		$a+=1;
		}
		?>
    </tbody>
	</table>
	<p>&nbsp;</p>
 </div>
 <script type="text/javascript">
       $(document).ready(function(){
		$('#lookup').dataTable({
		});			
	})

       $(function(){
            $(document).on('click','#buton',function(e){
                e.preventDefault();
                $.post('kreditmodal.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                ); $("#myModal").modal('show');
            });
        });
 </script>
</body>
</html>