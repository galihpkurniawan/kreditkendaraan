<?php 
require 'core/init.php';
$general->logged_out_protect();
session_start();
session_destroy();
header('Location:index.php');
?>