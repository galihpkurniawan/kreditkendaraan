<?php 
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
if($users['Level'] != "Customer")
{ 	exit("You don't have permission to access this page!"); }
if (isset($_POST['submit']))
{	
	if ($aju->aju_exists($users['ID_User'],$_POST['kdkendaraan']) == true)
	{
		$errors[] = 'Maaf, Jadwal dengan nama '.$_POST['nama'].' sudah tersedia dalam Database';
	}
	if (empty($errors) === true)
	{
	$kdkendaraan	= $_POST['kdkendaraan'];
	$waktu 			= strtotime(now);
	$status		= "Pengajuan";
	$aju->tambahaju($users['ID_User'],$waktu,$kdkendaraan,$status);
	header('Location: ajukredit.php');
	exit();
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Tambah Pengajuan Kredit</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css"/>
	<style type="text/css">
		body{background: #f7f7f7 url('images/body-bg.png');}
		.main-content{margin-top: 50px; width: 70%; margin-left: auto; margin-right: auto;}
		.eroran{margin: 20px;}
		fieldset {width: 100%;}
	</style>
	<script src="js/jquery-1.11.2.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<script src="js/jquery.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript"> 
		$(document).ready(function(){
			$("#waktu,#waktuup").datepicker
			({dateFormat:"dd-M-yy",changeMonth:true,changeYear:true,});
		});

			$(document).ready(function(){
			$("#kdkend").change(function(){
				var kdkend = $("#kdkend").val();
				$.ajax({
					url: "getharga.php",
					data: "kdkend="  + kdkend,
					dataType: 'json',
					cache: false,
				success: function(data){
					$("#harga").html(data.harga);
				}
			});
		});	
	});

var htmlobjek;
$(document).ready(function(){
	$("#jenis").change(function(){
    var kodejenis = $("#jenis").val();
    $.ajax({
        url: "carikendaraan.php",
        data: "kodejenis="+kodejenis,
        cache: false,
        success: function(msg){
            $("#kdkend").html(msg);
        }
    });
  });
});
	</script>
</head>
<body>	
	<ul class="breadcrumb navbar-fixed-top"><li>Customer</li><li><a href="ajukredit.php">Ajukan Kredit</a></li><li class="active">Tambah Ajukan Kredit</li></ul>
	<div class="main-content">
	<?php 
	if(empty($errors) === false){
		echo '<div class="eroran">'.
				   '<div class="alert alert-danger alert-error">'.
				        '<a href="#" class="close" data-dismiss="alert">&times;</a>'.
				       '<strong>Error! </strong>'.implode($errors). 
				   '</div>'.
				'</div>';
	}
	?>
	<div class="panel panel-primary">
	<div class="panel-heading">Form Pengajuan Kredit Kendaraan</div>
	<div class="panel-body">
	<form class="form-horizontal" role="form" name="satkerform" method="post" action="" onsubmit="return cekData();">
	<fieldset style="display: inline-block;">
	<legend> Tambah Pengajuan </legend>
				<div class="form-group">
					<label class="control-label col-sm-3"> Jenis Kendaraan : </label>
					<div class="col-sm-9">
						<select class="form-control" name='jenis' id="jenis" required>
							<option value="0">-- Pilih Jenis Kendaraan --</option>
							<?php 
							$jns=$aju->get_jns();
							foreach ($jns as $data)
							{
								echo '<option value="'.$data['Kd_Jenis'].'">'.$data['Nm_Jenis'].'</option>';
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-3"> Kendaraan : </label>
					<div class="col-sm-9">
						<select class="form-control" id="kdkend" name='kdkendaraan' required>
							<option value="0">-- Pilih Kendaraan --</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-3"> Harga : </label>
					<div class="col-sm-9">
						Rp. <label class="control-label" id="harga"> - </label>
					</div>
				</div>
				<div align="center">
						<input type='submit' class="btn btn-primary btn-lg" name='submit' value=' Save '>  
						<input type='reset' class="btn btn-warning btn-lg" name='reset' value=' Reset '> 
				</div> 
	</fieldset>
	</form>
	</div>
	</div>
</body>
</html>