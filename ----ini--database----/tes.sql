-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2016 at 08:38 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tes`
--

-- --------------------------------------------------------

--
-- Table structure for table `ajukredit`
--

CREATE TABLE IF NOT EXISTS `ajukredit` (
  `ID_Aju` int(11) NOT NULL AUTO_INCREMENT,
  `ID_User` int(11) NOT NULL,
  `Tgl_Aju` int(20) NOT NULL,
  `Status` varchar(30) NOT NULL,
  `Kd_Kendaraan` varchar(20) NOT NULL,
  `Status_Ptgs1` varchar(20) DEFAULT NULL,
  `Status_Ptgs2` varchar(20) DEFAULT NULL,
  `Status_Manager` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID_Aju`,`ID_User`,`Tgl_Aju`,`Kd_Kendaraan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ajukredit`
--

INSERT INTO `ajukredit` (`ID_Aju`, `ID_User`, `Tgl_Aju`, `Status`, `Kd_Kendaraan`, `Status_Ptgs1`, `Status_Ptgs2`, `Status_Manager`) VALUES
(1, 1, 1454360309, 'Pemeriksaan Manager', '20001', 'Approved', 'Approved', 'Cancel');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `ID_User` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) DEFAULT NULL,
  `City` varchar(20) DEFAULT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `Level` varchar(10) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Penghasilan` int(25) DEFAULT NULL,
  PRIMARY KEY (`ID_User`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`ID_User`, `Name`, `City`, `Country`, `Username`, `Password`, `Level`, `Email`, `Penghasilan`) VALUES
(1, 'Galih', 'Gombong', 'Indonesia', 'galih', 'galih', 'Customer', 'kukurnia77@gmail.com', NULL),
(2, 'Putra', 'Kebumen', 'Indonesia', 'putra', 'putra', 'Petugas1', 'kukurnia77@gmail.com', NULL),
(3, 'Kurniawan', 'Purwokerto', 'Indonesia', 'kurniawan', 'kurniawan', 'Petugas2', 'ibutiti45@gmail.com', NULL),
(4, 'Arka Farid Pratama', 'Gombong', 'Indonesia', 'arka', 'arka', 'Manager', 'arkafarid@gmail.com', NULL),
(5, 'Rizki', 'Kebumen', 'Indonesia', 'rizki', 'rizki', 'Customer', 'galih_cazanbe@yahoo.co.id', 5000000);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE IF NOT EXISTS `jenis` (
  `Kd_Jenis` int(11) NOT NULL,
  `Nm_Jenis` varchar(30) NOT NULL,
  PRIMARY KEY (`Kd_Jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`Kd_Jenis`, `Nm_Jenis`) VALUES
(1, 'Mobil'),
(2, 'Motor');

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE IF NOT EXISTS `kendaraan` (
  `Kd_Kendaraan` int(11) NOT NULL,
  `Kd_Jenis` int(11) NOT NULL,
  `Nm_Kendaraan` varchar(30) NOT NULL,
  `Harga` int(20) NOT NULL,
  PRIMARY KEY (`Kd_Kendaraan`,`Kd_Jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`Kd_Kendaraan`, `Kd_Jenis`, `Nm_Kendaraan`, `Harga`) VALUES
(10001, 1, 'Mobil Toyota Avanza', 150000000),
(20001, 2, 'Motor Kawasaki Ninja x2010', 50000000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
