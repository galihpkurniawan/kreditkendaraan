<?php 
//error_reporting(0);
require 'core/init.php';
$general->logged_in_protect();
if (isset($_GET['pesan']))
{
	$err[] = $_GET['pesan'];
}
if (empty($_POST) === false)
{	$username = strip_tags(addslashes(trim($_POST['Username']))); 
	$password = strip_tags(addslashes(trim($_POST['Password'])));
	$level = ($_POST['Level']); 
	if (empty($username) === true || empty($password) === true || $_POST['Level']=="") 
	{
		$errors[] = 'Mohon untuk mengisi semua kolom.';
	} 
	else if ($aju->user_exists($username) === false) 
	{
		$errors[] = 'Maaf Username/Password anda salah. Silakan coba lagi.';
	}
	else 
	{	$login = $aju->login($username,$password,$level);
		if ($login === false) {
			$errors[] = 'Maaf Username/Password anda salah. Silakan coba lagi.';
		}
		else
		{

				$_SESSION['loginid'] =  $login;
				echo $login;
				header('location: home.php');
				exit();
			
		}
		
	}
} 
?>
<!DOCTYPE html>
<html>
<head>
		<title>Login Aplikasi</title>
		<link rel="shortcut icon" href="images/iconia.ico" type="image/x-icon">
		<link rel="icon" href="images/iconia.ico" type="image/x-icon">
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
		<script src="js/jquery-1.11.2.min.js"></script>
    	<script src="bootstrap/js/bootstrap.js"></script>
		<meta charset="utf-8">
		<style type="text/css">
			.eroran{margin: 20px;}
			.errormsg { 		
				display: none;
                position: fixed;
                border: 1px rgba(255, 0, 0, 0.3);
                vertical-align: center;
				top: 10px;
                left:420px;
                padding: 5px 10px;
				border-radius:0.4em;
			  	border:1px solid #0A192A;
			  	box-shadow: 0 5px 10px 5px rgba(0,0,0,0.2);
               	background-color: rgba(255, 0, 0, 0.3);
                text-align: center;
                font-size:20px;color:#ffffff;}
		</style>
		<link href="css/loginstyle.css" rel='stylesheet' type='text/css' />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<script src="js/jquery-1.8.3.min.js"></script>
        <script>
//            angka 500 dibawah ini artinya pesan akan muncul dalam 0,5 detik setelah document ready
            $(document).ready(function(){setTimeout(function(){$(".errormsg").fadeIn('slow');}, 500);});
//            angka 3000 dibawah ini artinya pesan akan hilang dalam 3 detik setelah muncul
            setTimeout(function(){$(".errormsg").fadeOut('slow');}, 5000);
        </script> 
</head>
<body onload="document.login.username.focus();">
<body>
				<?php 
					if(empty($errors) === false){
					echo '<div class="errormsg">' . implode($errors) . '</div>';
					}
					?>
					<?php 
	if(empty($err) === false){
		echo '<div class="eroran">'.
				   '<div class="alert alert-warning alert-error">'.
				        '<a href="#" class="close" data-dismiss="alert">&times;</a>'.
				       '<strong></strong>'.implode($err). 
				   '</div>'.
				'</div>';
	}
	?>
	<div class="main">
		<form name="Login" method="post">
    		<h1><lable> Aplikasi Pengajuan Kredit Kendaraan</lable> </h1>
  			<div class="inset">
	  			<p>
	    		 <label for="email">Username</label>
   	 			<input type="text" name="Username" placeholder="" required/>
				</p>
  				<p>
				    <label for="password">Password</label>
				    <input type="password" name="Password" placeholder="" required/>
  				</p>
  				<p>
  					  <label for="password">Level</label>
				    <select name="Level">
				    <option value="">-- Pilih Level --</option>
					<option value="Customer">Customer</option>
					<option value="Petugas1">Petugas 1</option>
					<option value="Petugas2">Petugas 2</option>
					<option value="Manager">Manager</option>
					</select>
  				</p>
							   
				  
 			 </div>
 	 
			  <div class="tombol">
			    <input type="submit" name="login" value="Login">
			  </div>
			  <div class="footer">
			  <center>Galih @2016</center>
			  </div> 
		</form>
	</div> 
</body>
</html>