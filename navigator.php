<?php 
require 'core/init.php';
$general->logged_out_protect();
$users 		= $aju->userdata($_SESSION['loginid']);
?> 
<!DOCTYPE HTML>
<html>
<head>
	<title>Aplikasi Keuangan Daerah</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/leftmenustyles.css">
   <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
   <script src="js/script.js"></script>
   <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<script src="bootstrap/js/bootstrap.min.js"></script>
	<link rel=stylesheet type="text/css" href="css/style.css">
	<style type="text/css">
	body { background-color: #222222;}
	</style>
</head>
<body >
<div id="cssmenu">
<ul>
<li><a href="home.php" target="_parent"><span class="glyphicon glyphicon-home"> Home</span></a></li>
<?php
	if ($users['Level'] =="Customer")
	{	echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-user"> Customer</span></a>
					<ul>
 					 	<li><a href="profil.php" target="contentFrame"><span>Profil</span></a></li>
 					 	<li class="last"><a href="ajukredit.php" target="contentFrame"><span>Ajukan Kredit</span></a></li>';
			 echo '</ul>
				</li>
			';}
?>
<?php
	if ($users['Level'] =="Petugas1")
	{	echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-tasks"> Petugas 1</span></a>
					<ul>
 					 	<li><a href="periksa1.php" target="contentFrame"><span>Assignment</span></a></li>
 					 	<li><a href="allpengajuan1.php" target="contentFrame"><span>Resolved</span></a></li>
  						<li class="last"><a href="allpengajuan.php" target="contentFrame"><span>Semua Pengajuan</span></a></li>
					</ul>
				</li>
			';}
?>
<?php
	if ($users['Level'] =="Petugas2")
	{	echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-tasks"> Petugas 2</span></a>
					<ul>
 					 	<li><a href="periksa2.php" target="contentFrame"><span>Assignment</span></a></li>
 					 	<li><a href="allpengajuan2.php" target="contentFrame"><span>Resolved</span></a></li>
  						<li class="last"><a href="allpengajuan.php" target="contentFrame"><span>Semua Pengajuan</span></a></li>
					</ul>
				</li>
			';}
?>
<?php
	if ($users['Level'] =="Manager")
	{	echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-tasks"> Manager</span></a>
					<ul>
 					 	<li><a href="periksa3.php" target="contentFrame"><span>Assignment</span></a></li>
 					 	<li><a href="allpengajuan3.php" target="contentFrame"><span>Resolved</span></a></li>
  						<li class="last"><a href="allpengajuan.php" target="contentFrame"><span>Semua Pengajuan</span></a></li>
					</ul>
				</li>
			';}
?>
<?php 
if ($users['Level'] =="1" || $users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-briefcase"> Tahun Anggaran</span></a>
<ul>';
}
?>
<?php
	if ($users['Level'] =="1")
		{echo '
  <li><a href="trprogramdata.php" target="contentFrame"><span>TA Program</span></a></li>
  <li><a href="trkegiatandata.php" target="contentFrame"><span>TA Kegiatan</span></a></li>
  <li><a href="paguanggaranprogram.php" target="contentFrame"><span>Pagu Anggaran</span></a></li>
  ';}
	if ($users['Level'] =="1" || $users['Level'] =="3")
		{echo '
  <li><a href="belanjapilih.php" target="contentFrame"><span>Belanja</span></a></li>
  		';} ?>
<?php
  	if ($users['Level'] =="3")
		{echo '
  <li><a href="rkabelanjapilih.php" target="contentFrame"><span>RKA-SKPD</span></a></li>
  		';} ?>
 <?php
 if ($users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
  echo '<li><a href="bosharga.php" target="contentFrame"><span>BOS Pendamping</span></a></li>';} ?>
<?php
  	if ($users['Level'] =="3" || $users['Level'] =="2")
		{echo '  
  <li><a href="renjadata.php" target="contentFrame"><span>Renja</span></a></li>
  		';} ?>
<?php
if ($users['Level'] =="1" || $users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
echo '</ul>
</li>';
} ?>
<?php
	if ($users['Level'] == "1" || $users['Level'] == "2")
	{	echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-check"> Verifikasi</span></a>
					<ul>
					<li><a href="verifikasirka.php" target="contentFrame"><span>RKA-SKPD</span></a></li>
					</ul>
					</li>'; 
	}
?>
<?php
if ($users['Level'] =="1" || $users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
echo '<li class="has-sub"><a href="#"><span class="glyphicon glyphicon-print"> Laporan</span></a>
	<ul>';}?>
	<?php if ($users['Level'] == "1" || $users['Level'] == "2" || $users['Level'] == "3")
	{echo '<li><a href="laporanpilih.php" target="contentFrame"><span>RKA-SKPD</span></a></li>';} ?>
	<?php 
	if ($users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
		echo '<li class="last"><a href="lapbos.php" target="contentFrame"><span>Penerima BOS</span></a></li>';}?>
	<?php
	if ($users['Level'] =="1" || $users['Level'] =="2" || $users['Level'] =="3" || $users['Level'] =="4")
{
	echo '</ul>
</li>';} ?>
</ul>
</div>
</body>
</html>
