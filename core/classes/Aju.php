<?php
class Aju{
	private $db;
	public function __construct($database)
	{   $this->db = $database; }

	
	public function user_exists($username) 
	{	$query = $this->db->prepare("SELECT COUNT(`id_user`) FROM `customer` WHERE `username`= ?");
		$query->bindValue(1, $username);
		try
		{	$query->execute();
			$rows = $query->fetchColumn();
			if($rows == 1){
				return true;
			}else{
				return false;
			}
		} catch (PDOException $e){
			die($e->getMessage());
		}
	}
	public function login($username, $password, $level)
	{	$query = $this->db->prepare("SELECT `password`, `id_user`, `level` FROM `customer` WHERE `username` = ?");
		$query->bindValue(1, $username);
		try{
			$query->execute();
			$data 				= $query->fetch();
			$stored_password 	= $data['password'];
			$id   				= $data['id_user'];
			if($stored_password === $password && $level === $data['level']){
				return $id;	
			}else{
				return false;	
			}
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function userdata($id)
	{	$query = $this->db->prepare("SELECT * FROM `customer` WHERE `ID_User`= ?");
		$query->bindValue(1, $id);
		try{
			$query->execute();
			return $query->fetch();
		} catch(PDOException $e){
			die($e->getMessage());
		}
	} 
	public function get_aju_all()
	{	$query = $this->db->prepare("SELECT * FROM `ajukredit` order by `id_aju` asc");
		try{
			$query->execute();
		} catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll();
	}
	public function get_aju_by_cust($id)
	{	$query = $this->db->prepare("SELECT * FROM `ajukredit` WHERE `ID_User`= ?");
		$query->bindValue(1, $id);
		try{
			$query->execute();
		} catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll();
	}
	public function get_aju_by_status($status)
	{	$query = $this->db->prepare("SELECT * FROM `ajukredit` WHERE `Status`= ?");
		$query->bindValue(1, $status);
		try{
			$query->execute();
		} catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll();
	}
	public function get_aju_by_id($id)
	{	$query = $this->db->prepare("SELECT * FROM `ajukredit` WHERE `ID_Aju`= ?");
		$query->bindValue(1, $id);
		try{
			$query->execute();
			return $query->fetch();
		} catch(PDOException $e){
			die($e->getMessage());
		}
	} 
	public function kenddata($id)
	{	$query = $this->db->prepare("SELECT * FROM `kendaraan` WHERE `Kd_Kendaraan`= ?");
		$query->bindValue(1, $id);
		try{
			$query->execute();
			return $query->fetch();
		} catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function jenisdata($id)
	{	$query = $this->db->prepare("SELECT * FROM `jenis` WHERE `Kd_jenis`= ?");
		$query->bindValue(1, $id);
		try{
			$query->execute();
			return $query->fetch();
		} catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function tambahaju($iduser,$tglaju,$kdkendaraan,$status)
	{	$query 	= $this->db->prepare("INSERT INTO `ajukredit` (`id_user`,`tgl_aju`,`kd_kendaraan`,`status`) VALUES (?, ?, ?, ?)");
	 	$query->bindValue(1, $iduser);
		$query->bindValue(2, $tglaju); 	
		$query->bindValue(3, $kdkendaraan);
		$query->bindValue(4, $status);
	 	try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function updateaju_ptgs1($idaju,$status,$statusptgs1)
	{	$query 	= $this->db->prepare("UPDATE `ajukredit` SET `status`=?,`status_ptgs1`=? where id_aju=?");
	 	$query->bindValue(1, $status);
		$query->bindValue(2, $statusptgs1); 	
		$query->bindValue(3, $idaju);
	 	try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function updateaju_ptgs2($idaju,$status,$statusptgs2)
	{	$query 	= $this->db->prepare("UPDATE `ajukredit` SET `status`=?,`status_ptgs2`=? where id_aju=?");
	 	$query->bindValue(1, $status);
		$query->bindValue(2, $statusptgs2); 	
		$query->bindValue(3, $idaju);
	 	try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function updateaju_manager($idaju,$status,$statusmanager)
	{	$query 	= $this->db->prepare("UPDATE `ajukredit` SET `status`=?,`status_manager`=? where id_aju=?");
	 	$query->bindValue(1, $status);
		$query->bindValue(2, $statusmanager); 	
		$query->bindValue(3, $idaju);
	 	try{
			$query->execute();
		}catch(PDOException $e){
			die($e->getMessage());
		}
	}
	public function get_jns()
	{	$query = $this->db->prepare("SELECT * FROM `jenis`");
		try{
			$query->execute();
		} catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll();
	}
	public function get_kend_by_jenis($kodejenis)
	{	$query = $this->db->prepare("SELECT * FROM `kendaraan` WHERE `kd_jenis`= ? order by kd_kendaraan asc");
		$query->bindValue(1, $kodejenis);
		try{
			$query->execute();
		} catch(PDOException $e){
			die($e->getMessage());
		}
		return $query->fetchAll();
	}
	public function aju_exists($iduser,$kdkendaraan) 
	{	$query = $this->db->prepare("SELECT COUNT(`id_user`) FROM `ajukredit` WHERE `id_user`= ? and `kd_kendaraan`= ?");
		$query->bindValue(1, $iduser);
		$query->bindValue(2, $kdkendaraan);
		try
		{	$query->execute();
			$rows = $query->fetchColumn();
			if($rows == 1){
				return true;
			}else{
				return false;
			}
		} catch (PDOException $e){
			die($e->getMessage());
		}
	}
	
}