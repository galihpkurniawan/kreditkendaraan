<?php
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
$id=$_GET['id'];
$dataaju=$aju->get_aju_by_id($id);
$act=$_GET['act'];
$tuju=$aju->userdata($dataaju['ID_User']);
$kend=$aju->kenddata($dataaju['Kd_Kendaraan']);
$jenis=$aju->jenisdata($kend['Kd_Jenis']);

require_once "./swiftmailer-5.x/lib/swift_required.php";
$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
->setUsername('galihskripsi2015@gmail.com')
->setPassword('g4l1hputr4');


if ($act=="approve"){
	$aju->updateaju_manager($id,"Pemeriksaan Manager","Approved");
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance('Test Subject')
		->setFrom(array('galihskripsi2015@gmail.com' => 'galih'))
		->setTo(array($tuju['Email']))
		->setBody('Selamat Pengajuan Kredit Kendaraan anda '.$kend['Nm_Kendaraan'].' pada '.date('d-M-Y',$dataaju['Tgl_Aju']).' dengan harga Kendaraan Rp.'.number_format($kend['Harga'],0,",",".").', Disetujui, Segera hubungi kami.');
		$result = $mailer->send($message);
		if($result)
		{
		header('Location: periksa3.php');
		exit();
		}
}
else if ($act=="cancel"){
	$aju->updateaju_manager($id,"Pemeriksaan Manager","Cancel");
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance('Test Subject')
		->setFrom(array('galihskripsi2015@gmail.com' => 'galih'))
		->setTo(array($tuju['Email']))
		->setBody('Maaf Pengajuan Kredit Kendaraan anda '.$kend['Nm_Kendaraan'].' pada '.date('d-M-Y',$dataaju['Tgl_Aju']).' dengan harga Kendaraan Rp.'.number_format($kend['Harga'],0,",",".").', Tidak Disetujui.');
	$result = $mailer->send($message);
		if($result)
		{
		header('Location: periksa3.php');
		exit();
		}
}
?>