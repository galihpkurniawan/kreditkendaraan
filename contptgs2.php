<?php 
require 'core/init.php';
$general->logged_out_protect();
$users = $aju->userdata($_SESSION['loginid']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Aplikasi Pengajuan Kredit</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<script src="js/jquery-1.11.3.min.js"></script>
  	<script src="bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="datatables/dataTables.bootstrap.css"/>
     <link rel="stylesheet" href="css/backtop.css"/>
    <link href="css/homestyle.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="statistic/chartstyle.css" /> 
    
	<style type="text/css">
		body {
			margin: 10px;
    background: #f7f7f7 url('images/body-bg.png');}
    #page {margin-top : 40px; margin-left: auto; margin-right: auto; width : 95%;}
    .circle-wrap .stats-circle .glyphicon {width: 100%;}
    .alert {margin-right: 5px; margin-left: 5px; margin-top:50px;}
    .circle-wrap .stats-circle a {color: #fff; opacity: 30%;}
    .page-title{font-family: comic; opacity: 20%;}
    #top-link-block{opacity: 90%;}
    #footer #teks {color: black;}
	</style>
</head>
<body>
	<ul class="breadcrumb navbar-fixed-top"><li>Home</li></ul>
	 <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                         Selamat datang di <b>Aplikasi Pengajuan Kredit Kendaraan</b>
                    </div>
	<div id="page" class="dashboard">
						<h3 class="page-title">
							Dashboard
							<small>Petugas 2</small>
						</h3>
                    <!--BEGIN NOTIFICATION-->
                   
                    <!--END NOTIFICATION-->
                    <!-- BEGIN OVERVIEW STATISTIC BARS-->
                    <div class="row-fluid circle-state-overview">
                        
                        <div class="span2 responsive" data-tablet="span3" data-desktop="span2">
                            <div class="circle-wrap">
                                <div class="stats-circle red-color">
                                    <a href=""><i class="glyphicon glyphicon-th-large"></i></a>
                                </div>
                                <p>
                                    <strong></strong>
                                    Assignment
                                </p>
                            </div>
                        </div>
                        <div class="span3 responsive" data-tablet="span3" data-desktop="span2">
                            <div class="circle-wrap">
                                <div class="stats-circle green-color">
                                    <a href=""><i class="glyphicon glyphicon-inbox"></i></a>
                                </div>
                                <p>
                                    <strong></strong>
                                    Resolved
                                </p>
                            </div>
                        </div>
                        <div class="span2 responsive" data-tablet="span3" data-desktop="span2">
                            <div class="circle-wrap">
                                <div class="stats-circle gray-color">
                                    <a href=""><i class="glyphicon glyphicon-tags"></i></a>
                                </div>
                                <p>
                                    <strong></strong>
                                    Semua Pengajuan 
                                </p>
                            </div>
                        </div>
                        <div class="span3 responsive" data-tablet="span3" data-desktop="span2">
                            <div class="circle-wrap">
                                <div class="stats-circle purple-color">
                                    <a href=""><i class="glyphicon glyphicon-tag"></i></a>
                                </div>
                                <p>
                                    <strong></strong>
                                    Profile
                                </p>
                            </div>
                        </div>
                        <div class="span2 responsive" data-tablet="span3" data-desktop="span2">
                            <div class="circle-wrap">
                                <div class="stats-circle blue-color">
                                    <a href=""><i class="glyphicon glyphicon-usd"></i></a>
                                </div>
                                <p>
                                    <strong></strong>
                                    Help
                                </p>
                            </div>
                        </div>


                    </div>

     <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN SERVER LOAD PORTLET-->
                            <div class="widget">
                                <div class="widget-title">
                                    <h4><i class="glyphicon glyphicon-info-sign"></i> Daftar Assignment Petugas 2</h4>
                                </div>
                                <div class="widget-body">
                                <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Customer</th>
                                        <th>Kendaraan</th>
                                        <th>Jenis</th>
                                        <th>Harga</th>
                                        <th>Waktu Pengajuan</th>
                                        <th>Proses</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $info=$aju->get_aju_by_status("Pemeriksaan 1");
                                    $a=1;
                                    foreach ($info as $sch) {
                                        $us=$aju->userdata($sch['ID_User']);
                                        $kend=$aju->kenddata($sch['Kd_Kendaraan']);
                                        $jenis=$aju->jenisdata($kend['Kd_Jenis']);
                                        echo '<tr><td align="middle">'.$a.'</td>'.
                                             '<td>'.$us['Name'].'</td>'.
                                             '<td>'.$kend['Nm_Kendaraan'].'</td>'.
                                             '<td align="middle">'.$jenis['Nm_Jenis'].'</td>'.  
                                             '<td align="right">Rp. '.number_format($kend['Harga'],0,",",".").'</td>'.
                                             '<td align="middle">'.date('d-M-Y',$sch['Tgl_Aju']).'</td>'.
                                             '<td align="middle">'.$sch['Status'].'</td>'.
                                             '</tr>';
                                    $a+=1;
                                    }
                                    ?>
                                </tbody>
                                </table>
                                </div>
                            </div>
                            <!-- END SERVER LOAD PORTLET-->
                        </div>
    
                    </div>
    </div>
</body>
</html>